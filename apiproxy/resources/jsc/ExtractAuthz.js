var kmObj = context.getVariable("customer.kmObj");
var obj = JSON.parse(kmObj)
var km = obj.keymappings
for (var i= 0; i < km.length; i++) {
    var ith = km[i];
    var idtype = ith.idtype;
    var val = ith.value;
    context.setVariable("api.customerLookup.response." + idtype, val);
    print(idtype + "=" + context.getVariable("api.customerLookup.response." + idtype));
}