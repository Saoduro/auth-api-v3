var grants = "{\"grants\":[" +
                    "      {" +
                    "         \"billingId\":\"1234\"," +
                    "         \"assignedGrants\":{" +
                    "            \"grant\":\"Full-access" +
                    "         }," +
                    "         \"implicitGrants\":{" +
                    "            \"serviceAddresses\":[" +
                    "               {" +
                    "                  \"serviceId\":\"SA4567\"," +
                    "                  \"grant\":\"Full-access" +
                    "               }" +
                    "            ]" +
                    "         }" +
                    "      }" +
                    "   ]" +
                    "}"
context.setVariable("grants", grants);
