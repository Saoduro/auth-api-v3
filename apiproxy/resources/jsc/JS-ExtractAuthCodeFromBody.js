var input = context.getVariable("request.content");
var pkce = false;
var arr = input.split("&");
for (i = 0; i < arr.length; i++) {
  var pair = arr[i];
  var nameValue = pair.split("=");
  var key = nameValue[0];
  var value = nameValue[1];
  print(key + "=" + value);
  if(key == "redirect_uri"){
      value = decodeURIComponent(value);
  }
  if(key == "code_verifier"){
      pkce = true; //flag to determine if an empty code verifier needs to be set for non-pkce requests. see below.
  }
  context.setVariable("authorize.request." + key, value);
}

if(!pkce){
    context.setVariable("authorize.request.code_verifier", ""); //set an empty code verifier for requests that are not using pkce. Code verifier MUST be present otherwise the variable will not resolve in the service callout to gigya.
}